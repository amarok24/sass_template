# Sass starter template

A dummy project with HTML + SCSS (Sass), some CSS classes and breakpoints inspired by Bootstrap CSS framework.

## Usage
Make sure you have [Dart Sass](https://sass-lang.com/dart-sass/) or a compatible Sass compiler on your PC.

Run the shell scripts `sasswatch.sh` and `sassbuild.sh` as needed.

## License

License: The Unlicense. For more information, please refer to
http://unlicense.org\
(A license with no conditions whatsoever which dedicates works to the public
domain. Unlicensed works, modifications, and larger works may be distributed
under different terms and without source code.)
