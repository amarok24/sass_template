<?php
/*
 * Sass starter template
 * Version: 1.0.1
 *
 * Author: Jan Prazak
 * Project repository: https://codeberg.org/amarok24/sass_starter
 * License: The Unlicense, http://unlicense.org
 */
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport"
		  content="width=device-width, initial-scale=1.0" />
	<title>Sass starter template</title>
	<link rel="stylesheet"
		  href="dist/style.css" />
</head>

<body>
	<main class="container max-lg">
		<h1>Sass starter template</h1>

		<p>
			Project source:<br />
			<a href="https://codeberg.org/amarok24/sass_template">https://codeberg.org/amarok24/sass_template</a>
		</p>

		<form action=""
			  method="get">
			<label>
				<input type="checkbox"
					   name=""
					   id="" /> Checkbox
			</label>
			<br />
			<label>
				<input type="range"
					   id="quantity"
					   name="quantity"
					   min="0"
					   max="100"
					   step="5"
					   value="20" />
				quantity <output id="quantity_val"></output>
			</label>
			<br />
			<label>
				<input type="password"
					   required=""
					   name=""
					   id="" /> password
			</label>
			<br />
			<label>
				<input type="email"
					   pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}"
					   required=""
					   name=""
					   id="" />
				e-mail
			</label>
			<br /><br />
			<input type="reset"
				   value="Reset button" />
			<input type="submit"
				   value="Submit button"
				   class="icon-submit" /><br />
		</form>

		<br />
		<input type="button"
			   id="toggle_light"
			   value="light/dark" />
	</main>

	<script>
		const quantity_val = document.getElementById('quantity_val');
		const quantity_input = document.getElementById('quantity');
		const toggle_light = document.getElementById('toggle_light');
		const prefers_dark = window.matchMedia('(prefers-color-scheme: dark)');

		quantity_val.textContent = quantity_input.value;
		quantity_input.addEventListener('input', (ev) => {
			quantity_val.textContent = ev.target.value;
		});

		toggle_light.addEventListener('click', (ev) => {
			if (prefers_dark)
				document.body.classList.toggle('force_lightmode');
			else
				document.body.classList.toggle('force_darkmode');
			ev.preventDefault();
		});
	</script>

	</bod>

</html>
